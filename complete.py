import xml.etree.cElementTree as ET
def addressMenu():
    print("")
    print("Address  Database")
    print("1 -Read all addresses")
    print("2 -Update a address")
    print("3 -Create a address")
    print("4 -Delete a address")
    print("x -Exit")
    userinput=input("Choose option 1-5 or 'x' to exit: ")
    return userinput

# read address

def readaddress():
    for contact in root.findall('contact'):
        name = contact.find('name').text
        address = contact.find('address').text
        phone = contact.find('phone').text
        age = contact.find('age').text
        email= contact.find('email').text
        id= contact.find('id').text
        print("ID: "+ id)        
        print("Name: "+ name)
        print("Address: "+ address)
        print("Phone: "+ phone)
        print("Email: "+ email)
        print("Age: "+ age)
    print("End of list")

# record exists
def recordExists(email):

    #set the default return value to false
    retValue=False

    for contact in root.findall('contact'):
        emailb = contact.find('email').text
        
        if emailb==email:
            retValue=True      
    return retValue


# updateaddress
def updateaddress():
    #ask the user for the id to edit 
    email =input("Enter the Email address  to edit: ")
    for contact in root.findall('contact'):
        emailb = contact.find('email').text

        if emailb==email:
            id = contact.find('id').text
            name = contact.find('name').text
            address = contact.find('address').text
            phone = contact.find('phone').text
            email = contact.find('email').text
            age = contact.find('age').text

            #update name
            name = input("Please enter name:(" + name + ")") or name
            contact.find('name').text= name
            #update address
            address = input("Please enter address:(" + address + ")") or address
            contact.find('address').text= address
            #update phone
            phone = input("Please enter phone:(" + phone + ")") or phone
            contact.find('phone').text= phone
            #update email
            phone = input("Please enter email:(" + email + ")") or email
            contact.find('email').text= email
            #update age
            age = input("Please enter age:(" + age + ")") or age
            contact.find('age').text= age
            
            #save the update
            ntree.write("address.xml")            
            #print the field values
            print("RECORD ID: "+ id + " UPDATED")            

            
# newid
def newid():
    maxid = 0
    for contact in root.findall('contact'):
        id= int(contact.find('id').text)
        if id>maxid:
            maxid=id
    return maxid+1

# createaddress
def createaddress():
    #add a record-but we would need to check for duplicates first
    #ask the user for the email address to search for
    email =input("Enter the Email address you want to search for: ")    
    exists=recordExists(email)
    if exists==False:
        
        #get a new id
        nid=newid()

        #get the field values from the user
        print("Create a record")
        name=input("Name:")
        address=input("Address:")
        age=input("Age:")
        email=input("Email:")
        phone=input("Phone:")

        #create a contact element 
        newrecord = ET.SubElement(root, "contact",id=str(nid))

        #add the fields into out new record
        ET.SubElement(newrecord, "id", name="id").text = str(nid)
        ET.SubElement(newrecord, "name", name="name").text = name
        ET.SubElement(newrecord, "address", name="address").text = address
        ET.SubElement(newrecord, "phone", name="phone").text = phone
        ET.SubElement(newrecord, "email", name="email").text = email
        ET.SubElement(newrecord, "age", name="age").text = age

        #finally save the update
        ntree.write("address.xml")    

    else:
        print("Record already exists")

# deleteaddress
def deleteaddress():

    #get a variable for the record that you want to delete
    deleterecord=input("Enter the Email address of the record you want to delete: ")

    for contact in root.findall('contact'):
        #get the email of the current record, 
        emailb= contact.find('email').text
        if emailb == deleterecord:
            root.remove(contact)            
            #finally save the update
            ntree.write("address.xml")        

# main program execution starts here open/read the entire database
ntree = ET.parse('address.xml')
root = ntree.getroot()

userinput=""
while userinput !="x":    
    userinput=addressMenu()

    if userinput =="1":
        readaddress()

    if userinput =="2":
        updateaddress()

    if userinput =="3":
        createaddress()

    if userinput =="4":
        deleteaddress()
