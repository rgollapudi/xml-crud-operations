import xmltodict

# Open the file and read the contents
with open('address.xml', 'r', encoding='UTF-8') as file:
    read_file = file.read()
  
# Use xmltodict to parse and convert the XML document to dictionary
dict = xmltodict.parse(read_file)
print(dict)