import xml.etree.ElementTree as ET
tree = ET.parse('updated_output.xml')
root = tree.getroot()
# searches for all the possible mails 
for addresses in root.findall('addresses'): 
    del_data= int(addresses.find('age').text)
    # if the mail matches with the input mail, it deletes using remove function
    if del_data > 24:
        root.remove(addresses)    
tree.write("updated_output.xml")        

